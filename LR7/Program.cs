﻿using System;
using System.Linq;

namespace LR7
{
    class Program
    {
        class Fraction : IComparable
        {
            public int numerator = default;
            public int denominator = 1;
            int sign = 1;
            public char[] separators = { ' ', '/', '-', ':' };

            public Fraction(int up, int low) => (numerator, denominator) = (up, low);
            public Fraction(string str)
            {
                string[] arrOfString = str.Split(separators);
                numerator = Int32.Parse(arrOfString[0]);
                denominator = Int32.Parse(arrOfString[1]);
                if (numerator * denominator > 0)
                {
                    sign = 1;
                }
                else
                {
                    sign = -1;
                }
                PrintFractiom();
            }
            public Fraction(int value)
            {
                numerator = value;
                if (numerator > 0)
                {
                    sign = 1;
                }
                else
                {
                    sign = -1;
                }
                PrintFractiom();
            }
            public void PrintFractiom()
            {
                Console.WriteLine($"Плученная дробь {this.numerator}/{this.denominator}");
            }
            private static int getNOD(int a, int b)
            {
                while (b != 0)
                {
                    int temp = b;
                    b = a % b;
                    a = temp;
                }
                return a;
            }
            private static int getNOK(int a, int b)
            {
                return a * b / getNOD(a, b);
            }
            private static Fraction performOperation(Fraction a, Fraction b, Func<int, int, int> operation)
            {
                int NOK = getNOK(a.denominator, b.denominator);
                int fractionFactor1 = NOK / a.denominator;
                int fractionFactor2 = NOK / b.denominator;
                int result = operation(a.numerator * fractionFactor1, b.numerator * fractionFactor2);
                return new Fraction(result, a.denominator * fractionFactor1);
            }
            public static Fraction operator +(Fraction a, Fraction b)
            {
                return performOperation(a, b, (int x, int y) => x + y);
            }
            public static Fraction operator +(Fraction a, int b)
            {
                return a + new Fraction(b);
            }
            public static Fraction operator -(Fraction a, Fraction b)
            {
                return performOperation(a, b, (int x, int y) => x - y);
            }
            public static Fraction operator -(Fraction a, int b)
            {
                return a - new Fraction(b);
            }
            public static Fraction operator *(Fraction a, Fraction b)
            {
                return new Fraction(a.numerator * a.sign * b.numerator * b.sign, a.denominator * b.denominator);
            }
            public static Fraction operator /(Fraction a, Fraction b)
            {
                return a * b.GetReverse();
            }
            public static Fraction operator /(Fraction a, int b)
            {
                return a / new Fraction(b);
            }
            public static Fraction operator ++(Fraction a)
            {
                return a + 1;
            }
            public static Fraction operator --(Fraction a)
            {
                return a - 1;
            }
            private Fraction GetReverse()
            {
                return new Fraction(this.denominator * this.sign, this.numerator);
            }
            public Fraction Reduce()
            {
                Fraction result = this;
                int greatestCommonDivisor = getNOD(this.numerator, this.denominator);
                result.numerator /= greatestCommonDivisor;
                result.denominator /= greatestCommonDivisor;
                return result;
            }
            public override string ToString()
            {
                int[] arr = { 1, 2, 5 ,10};
                if (this.numerator == 0)
                {
                    return "0";
                }
                string result;
                if (this.sign < 0)
                {
                    result = "-";
                }
                else
                {
                    result = "";
                }
                if (this.numerator == this.denominator)
                {
                    return result + "1";
                }
                if (this.denominator == 1)
                {
                    return this.numerator.ToString();
                }
                int ChekDenominator = denominator % 10;
                if (ChekDenominator % 10 == 0 && arr.Contains(ChekDenominator))
                {
                    double dval = this.numerator / this.denominator;
                    return result + dval;
                }
                return this.numerator + "/" + this.denominator; 
            }
            public bool Equals(Fraction obj)
            {
                Fraction a = this.Reduce();
                Fraction b = obj.Reduce();
                return a.numerator == b.numerator &&
                a.denominator == b.denominator &&
                a.sign == b.sign;
            }

            public static bool operator ==(Fraction a, Fraction b)
            {
                return a.Equals(b);
            }
            public static bool operator !=(Fraction a, Fraction b)
            {
                return !(a == b);
            }
            public override int GetHashCode()
            {
                return this.sign * (this.numerator * this.numerator + this.denominator * this.denominator);
            }

            public int CompareTo(object obj)
            {
                if (this.Equals(obj))
                {
                    return 0;
                }
                Fraction a = this.Reduce();
                Fraction b = (Fraction)obj;
                b = b.Reduce();
                if (a.numerator * a.sign * b.denominator > b.numerator * b.sign * a.denominator)
                {
                    return 1;
                }
                return -1;
            }

            public static bool operator <(Fraction a, Fraction b)
            {
                return a.CompareTo(b) < 0;
            }
            public static bool operator >(Fraction a, Fraction b)
            {
                return a.CompareTo(b) > 0;
            }


        }
        static void Main(string[] args)
        {
            Fraction obj1 = new Fraction("1:2");
            Fraction obj2 = new Fraction(10);
            Fraction obj3 = new Fraction(1, 2);
            Fraction objAddition =obj1 + obj3;
            objAddition.PrintFractiom();
            Fraction objMultipe = obj1 * obj2;
            objMultipe.PrintFractiom();
            Fraction objDivision = obj1 / obj2;
            objDivision.PrintFractiom();
            Console.WriteLine(objAddition.ToString());
            Console.WriteLine(obj3.ToString());


        }
    }
}
